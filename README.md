# DWH Project - Row and column store performance comparison, using Star Schema Benchmark and Microsoft SQL Server

Jeremi Kiraga, Wojciech Sycz

Poznan University of Technology

<https://bitbucket.org/kiragaj/projekt-hd/src/master/>

- [DWH Project - Row and column store performance comparison, using Star Schema Benchmark and Microsoft SQL Server](#dwh-project---row-and-column-store-performance-comparison-using-star-schema-benchmark-and-microsoft-sql-server)
  - [Project goal and description](#project-goal-and-description)
  - [Development](#development)
    - [Generating SSB example data](#generating-ssb-example-data)
    - [Microsoft SQL Server](#microsoft-sql-server)
      - [Awaiting for the database](#awaiting-for-the-database)
    - [DDL](#ddl)
    - [Importing data](#importing-data)
      - [Using SQL Management Studio Wizard](#using-sql-management-studio-wizard)
      - [Using `dtexec`](#using-dtexec)
    - [Keys and indexes](#keys-and-indexes)
    - [Benchmark queries](#benchmark-queries)
  - [Experiment](#experiment)
    - [Benchmarking](#benchmarking)
    - [Test environment](#test-environment)
    - [Database size](#database-size)
    - [Query execution time](#query-execution-time)
  - [Summary](#summary)
  - [Bibliography](#bibliography)

## Project goal and description

The main goal of the project is a performance comparison of a row and column data storage in Microsoft SQL Server as well as a development of a fully automatized experiment, that can be executed on any configuration with Docker Linux installed.

For performance comparison, the Star Schema Benchmark will be used:

> The SSB is designed to measure the performance of database products in support of classical data ware-housing applications, and is based on the TPC-H benchmark, modified in a number of ways (…).
>
>1. We combine the TPC-H LINEITEM and ORDERS tables into one sales fact table that we name LINEORDER. This denormalization is standard in warehousing (…), and makes many joins unnecessary in common queries.
>1. We drop the PARTSUPP table since it would belong to a different data mart than the ORDERS and LINEITEM information. This is because PARTSUPP has different temporal granularity (…).
>1. We drop the comment attribute of a LINEITEM (27 chars), the comment for an order (49 chars), and the shipping instructions for a LINEITEM (25 chars) because a warehouse does not store such information in a fact table (they can’t be aggregated, and take significant storage).
>1. We add the DATE dimension table, as is standard for a warehouse on sales.

According to Microsoft documentation:

- row store definition

>A row store is data that's logically organized as a table with rows and columns, and physically stored in a row-wise data format. This format is the traditional way to store relational table data. In SQL Server, row store refers to a table where the underlying data storage format is a heap, a clustered index, or a memory-optimized table.

- column store definition

>A column store index is a technology for storing, retrieving, and managing data by using a columnar data format, called a column store. A column store is data that's logically organized as a table with rows and columns, and physically stored in a column-wise data format.

## Development

### Generating SSB example data

Up-to-date Star Schema Benchmark data generator is available here: <https://github.com/eyalroz/ssb-dbgen/>.

While it is not the original source code, the provided repository is adjusting the original project to modern CPU architecture (64-bit support) as well as providing the compilation process working out-of-the-box.

SSB example data can be generated using `./data/Dockerfile` and `./data/docker-compose.yml` files.
All output files will be produced to `./ssb_output` directory.

`./data/Dockerfile`:

```Dockerfile
FROM rikorose/gcc-cmake:gcc-9

RUN mkdir /output && mkdir /ssb

WORKDIR /ssb
RUN git clone https://github.com/eyalroz/ssb-dbgen/ .
RUN cmake DATABASE=SQLSERVER . && cmake --build .

COPY entrypoint.sh entrypoint.sh
RUN chmod +x entrypoint.sh
ENTRYPOINT [ "./entrypoint.sh" ]
```

`./data/docker-compose.yaml`:

```yaml
version: "2.4"

services:
  sbb:
    image: ssb-dbgen
    build:
      context: .
      dockerfile: Dockerfile
    environment:
      - SBB_SCALE=1
    volumes:
        - ../ssb_output/:/output
```

SSB scale (effectively defining the size of the test data set) can be set by alteration of the `SBB_SCALE` environment variable in `./data/docker-compose.yml` file.

To start the generation process, execute the following script:

```bash
cd ./data/
docker-compose up && docker-compose down
```

### Microsoft SQL Server

Following script can be used to start Microsoft SQL Server:

```bash
cd ./database/
docker-compose up -d
```

SQL Server will start running in the background.

The default `sa` password is `yourStrong(!)Password`. The password can be altered by `./database/docker-compose.yaml` file modification.

#### Awaiting for the database

To allow full automation of the benchmark process, small script to wait for the database to be ready has been created and can be found in `./await_for_database/` directory.

`./await_for_database/await_for_database.sh`:

```bash
#!/bin/bash

sleep_time="1s"
retries=0
while sqlcmd -S $SQL_SERVER -U sa -P $SA_PASSWORD -Q "SELECT 1" >> /dev/null; ret=$?; retries=$((retries+1)); [[ ret -ne 0 ]]; do
    echo "Try $retries: database not ready to accept connections, sleeping for $sleep_time"
    sleep $sleep_time
done

echo "Try $retries: database is ready to accept connections"

sleep 30s

exit 0
```

`./await_for_database/Dockerfile`:

```Dockerfile
FROM mcr.microsoft.com/mssql-tools
ENV PATH="/opt/mssql-tools/bin:$PATH"
WORKDIR /
RUN curl https://raw.githubusercontent.com/vishnubob/wait-for-it/master/wait-for-it.sh --output wait-for-it.sh && chmod +x wait-for-it.sh
COPY await_for_database.sh .
RUN chmod +x await_for_database.sh
CMD ./wait-for-it.sh -t 30 sqlserver:1433 --strict -- ./await_for_database.sh
```

`./await_for_database/docker-compose.yaml`:

```yaml
version: "2.4"

services:
  await_for_database:
      image: await_for_database
      build:
        context: .
        dockerfile: Dockerfile
      environment:
        - 'SQL_SERVER=sqlserver'
        - 'SA_PASSWORD=yourStrong(!)Password'
      networks:
        - ssb_network
networks:
  ssb_network:
    external:
      name: ssb_network
```

### DDL

Initially, 3 different approaches has been selected for benchmarking:

- row store,
- column store
- column store with compression.

However, the initial execution of the benchmark let to the conclusion, that the column store with compression is difficult to test in a deterministic way. Microsoft SQL Server is not enabling end-users to force DBMS to compress data. During benchmarking, it has been noticed, that compression is started in a random moment, what is affecting the final benchmark results.
Based on this observation, the column store with compression has been removed from further investigation.

2 DDL scripts has been created to reflect 2 different storage approaches:

- row store: `./dll/setup_rowStore.sql`

```sql
USE [master];
GO

DROP DATABASE IF EXISTS PROJEKT_HD_ROWSTORE;
GO

CREATE DATABASE PROJEKT_HD_ROWSTORE;
GO

USE PROJEKT_HD_ROWSTORE;
GO
ALTER DATABASE PROJEKT_HD_ROWSTORE SET RECOVERY SIMPLE;
GO
CREATE TABLE PART
(
    P_PARTKEY int NOT NULL,
    P_NAME varchar(50),
    P_MFGR varchar(50),
    P_CATEGORY varchar(50),
    P_BRAND1 varchar(50),
    P_COLOR varchar(50),
    P_TYPE varchar(50),
    P_SIZE numeric,
    P_CONTAINER varchar(50)
);

CREATE TABLE SUPPLIER
(
    S_SUPPKEY int NOT NULL,
    S_NAME varchar(50),
    S_ADDRESS varchar(50),
    S_CITY varchar(50),
    S_NATION varchar(50),
    S_REGION varchar(50),
    S_PHONE varchar(50)
);

CREATE TABLE CUSTOMER
(
    C_CUSTKEY int NOT NULL,
    C_NAME varchar(50),
    C_ADDRESS varchar(50),
    C_CITY varchar(50),
    C_NATION varchar(50),
    C_REGION varchar(50),
    C_PHONE varchar(50),
    C_MKTSEGMENT varchar(50),
);

CREATE TABLE DATE
(
    D_DATEKEY int NOT NULL,
    D_DATE varchar(50),
    D_DAYOFWEEK varchar(50),
    D_MONTH varchar(50),
    D_YEAR int,
    D_YEARMONTHNUM int,
    D_YEARMONTH varchar(50),
    D_DAYNUMINWEEK int,
    D_DAYNUMINMONTH int,
    D_DAYNUMINYEAR int,
    D_MONTHNUMINYEAR int,
    D_WEEKNUMINYEAR int,
    D_SELLINGSEASON varchar(50),
    D_LASTDAYINWEEKFL bit,
    D_LASTDAYINMONTHFL bit,
    D_HOLIDAYFL bit,
    D_WEEKDAYFL bit
);

CREATE TABLE LINEORDER
(
    LO_ORDERKEY int NOT NULL,
    LO_LINENUMBER int NOT NULL,
    LO_CUSTKEY int,
    LO_PARTKEY int,
    LO_SUPPKEY int,
    LO_ORDERDATE int,
    LO_ORDERPRIORITY varchar(50),
    LO_SHIPPRIORITY varchar(50),
    LO_QUANTITY numeric,
    LO_EXTENDEDPRICE numeric,
    LO_ORDTOTALPRICE numeric,
    LO_DISCOUNT numeric,
    LO_REVENUE numeric,
    LO_SUPPLYCOST numeric,
    LO_TAX numeric,
    LO_COMMITDATE int,
    LO_SHIPMODE varchar(50)
);

GO
```

- column store: `./dll/setup_columnStore.sql`

```sql
USE [master];
GO

DROP DATABASE IF EXISTS PROJEKT_HD_COLUMNSTORE;
GO

CREATE DATABASE PROJEKT_HD_COLUMNSTORE;
GO
USE PROJEKT_HD_COLUMNSTORE;
GO
ALTER DATABASE PROJEKT_HD_COLUMNSTORE SET RECOVERY SIMPLE;
GO
CREATE TABLE PART
(
    P_PARTKEY int NOT NULL,
    P_NAME varchar(50),
    P_MFGR varchar(50),
    P_CATEGORY varchar(50),
    P_BRAND1 varchar(50),
    P_COLOR varchar(50),
    P_TYPE varchar(50),
    P_SIZE numeric,
    P_CONTAINER varchar(50)
);
GO
CREATE CLUSTERED COLUMNSTORE INDEX Part_ColumnIndex ON PART
WITH ( MAXDOP = 1
)
GO
CREATE TABLE SUPPLIER
(
    S_SUPPKEY int NOT NULL,
    S_NAME varchar(50),
    S_ADDRESS varchar(50),
    S_CITY varchar(50),
    S_NATION varchar(50),
    S_REGION varchar(50),
    S_PHONE varchar(50)
);
GO
CREATE CLUSTERED COLUMNSTORE INDEX Supplier_ColumnIndex ON SUPPLIER
WITH ( MAXDOP = 1
)
GO
CREATE TABLE CUSTOMER
(
    C_CUSTKEY int NOT NULL,
    C_NAME varchar(50),
    C_ADDRESS varchar(50),
    C_CITY varchar(50),
    C_NATION varchar(50),
    C_REGION varchar(50),
    C_PHONE varchar(50),
    C_MKTSEGMENT varchar(50),
);
GO
CREATE CLUSTERED COLUMNSTORE INDEX Customer_ColumnIndex ON CUSTOMER
WITH ( MAXDOP = 1
)
GO
CREATE TABLE DATE
(
    D_DATEKEY int NOT NULL,
    D_DATE varchar(50),
    D_DAYOFWEEK varchar(50),
    D_MONTH varchar(50),
    D_YEAR int,
    D_YEARMONTHNUM int,
    D_YEARMONTH varchar(50),
    D_DAYNUMINWEEK int,
    D_DAYNUMINMONTH int,
    D_DAYNUMINYEAR int,
    D_MONTHNUMINYEAR int,
    D_WEEKNUMINYEAR int,
    D_SELLINGSEASON varchar(50),
    D_LASTDAYINWEEKFL bit,
    D_LASTDAYINMONTHFL bit,
    D_HOLIDAYFL bit,
    D_WEEKDAYFL bit
);
GO
CREATE CLUSTERED COLUMNSTORE INDEX Date_ColumnIndex ON DATE
WITH ( MAXDOP = 1
)
GO
CREATE TABLE LINEORDER
(
    LO_ORDERKEY int NOT NULL,
    LO_LINENUMBER int NOT NULL,
    LO_CUSTKEY int ,
    LO_PARTKEY int ,
    LO_SUPPKEY int ,
    LO_ORDERDATE int,
    LO_ORDERPRIORITY varchar(50),
    LO_SHIPPRIORITY varchar(50),
    LO_QUANTITY numeric,
    LO_EXTENDEDPRICE numeric,
    LO_ORDTOTALPRICE numeric,
    LO_DISCOUNT numeric,
    LO_REVENUE numeric,
    LO_SUPPLYCOST numeric,
    LO_TAX numeric,
    LO_COMMITDATE int ,
    LO_SHIPMODE varchar(50),
);
GO
CREATE CLUSTERED COLUMNSTORE INDEX LineOrder_ColumnIndex ON LINEORDER
WITH ( MAXDOP = 1
)
GO
```

Note, that each script will drop the database if such a database exists.

### Importing data

#### Using SQL Management Studio Wizard

Importing example data can be done using the Import wizard, available in SQL Server Management Studio.

![01.png](importing_csv/01.png)

Select `Flat File Source` as a data source. Adjust `Code page` to follow database settings (in case of docker it should be selected to `1252`). Test qualifier should be set to `"`. The first row does not contains column headers.

![02.png](importing_csv/02.png)

Next step allows to check column detected in the input file.
Delimiter should be set to `|`.

![03.png](importing_csv/03.png)

The destination should be set to `SQL Server Native Client 11.0`, to import data to the localhost database server. In the case of Docker instance of SQL Server, SQL account should be used.

![04.png](importing_csv/04.png)

For each column in input flat files, column mapping need to be defined. The dropdown list is reflecting data order in flat files one-to-one, therefore for each column in the flat file, the first column from the dropdown list should be selected.

![05.png](importing_csv/05.png)

Error handling should be set to `Fail` to ensure data consistency during import.

![06.png](importing_csv/06.png)

`Save package` option can be used for reusability. Keep in mind, that SQL Server Integration Services need to be used to re-run saved packages.

![07.png](importing_csv/07.png)

Summary of planned operation will be displayed. `Finish` button will start ETL operation.

![08.png](importing_csv/08.png)

The import wizard will perform a certain step. In the case of data inconsistency, an error message will be displayed and no data will be imported. Successful import will look similar to results on the picture bellow.

![09.png](importing_csv/09.png)

#### Using `dtexec`

All packages has been also saved as SSIS packages in `./ssis` directory.

Helper Docker container has been developed to enable automated data loading.

`./ssis/load_data.sh`

```bash
#!/bin/bash

dtexec /F /packages/Date.dtsx /CONN "DestinationConnectionOLEDB"\;"\"Data Source=sqlserver;User ID=sa;Initial Catalog=${INITIAL_CATALOG};Password=yourStrong(!)Password;Provider=SQLNCLI11;Auto Translate=false;\""
dtexec /F /packages/Customer.dtsx /CONN "DestinationConnectionOLEDB"\;"\"Data Source=sqlserver;User ID=sa;Initial Catalog=${INITIAL_CATALOG};Password=yourStrong(!)Password;Provider=SQLNCLI11;Auto Translate=false;\""
dtexec /F /packages/Part.dtsx /CONN "DestinationConnectionOLEDB"\;"\"Data Source=sqlserver;User ID=sa;Initial Catalog=${INITIAL_CATALOG};Password=yourStrong(!)Password;Provider=SQLNCLI11;Auto Translate=false;\""
dtexec /F /packages/Supplier.dtsx /CONN "DestinationConnectionOLEDB"\;"\"Data Source=sqlserver;User ID=sa;Initial Catalog=${INITIAL_CATALOG};Password=yourStrong(!)Password;Provider=SQLNCLI11;Auto Translate=false;\""
dtexec /F /packages/Lineorder.dtsx /CONN "DestinationConnectionOLEDB"\;"\"Data Source=sqlserver;User ID=sa;Initial Catalog=${INITIAL_CATALOG};Password=yourStrong(!)Password;Provider=SQLNCLI11;Auto Translate=false;\""
```

`./ssis/Dockerfile`:

```Dockerfile
FROM ubuntu:latest
RUN apt-get update && apt-get upgrade -y
RUN apt-get install -y \
    curl \
    gnupg \
    software-properties-common
RUN curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add -
RUN add-apt-repository "$(curl https://packages.microsoft.com/config/ubuntu/16.04/mssql-server-2019.list)"
RUN apt-get install -y mssql-server-is
RUN SSIS_PID=Developer ACCEPT_EULA=Y /opt/ssis/bin/ssis-conf -n setup; exit 0
ENV PATH=/opt/ssis/bin:$PATH
COPY ./packages /packages
COPY load_data.sh /load_data.sh
RUN chmod +x /load_data.sh
CMD /load_data.sh
```

`./ssis/docker-compose.yaml`

```yaml
version: "2.4"

services:
  ssis:
      image: ssis
      build:
        context: .
        dockerfile: Dockerfile
      networks:
        - ssb_network
      volumes:
        - ../ssb_output/:/input/
networks:
  ssb_network:
    external:
      name: ssb_network
```

### Keys and indexes

To improve data loading performance, primary and foreign keys as well as indexes has been created after data importing with the following queries:

`./dll/primaryKeys.sql`

```sql
USE PROJEKT_HD_ROWSTORE;
GO

ALTER TABLE PART ADD PRIMARY KEY (P_PARTKEY);
ALTER TABLE SUPPLIER ADD PRIMARY KEY (S_SUPPKEY);
ALTER TABLE CUSTOMER ADD PRIMARY KEY (C_CUSTKEY);
ALTER TABLE [DATE] ADD PRIMARY KEY (D_DATEKEY);
ALTER TABLE LINEORDER ADD PRIMARY KEY (LO_ORDERKEY, LO_LINENUMBER);

USE PROJEKT_HD_COLUMNSTORE;
GO

ALTER TABLE PART ADD PRIMARY KEY (P_PARTKEY);
ALTER TABLE SUPPLIER ADD PRIMARY KEY (S_SUPPKEY);
ALTER TABLE CUSTOMER ADD PRIMARY KEY (C_CUSTKEY);
ALTER TABLE [DATE] ADD PRIMARY KEY (D_DATEKEY);
ALTER TABLE LINEORDER ADD PRIMARY KEY (LO_ORDERKEY, LO_LINENUMBER);

USE PROJEKT_HD_COLUMNSTORE_COMPRESSION;
GO

ALTER TABLE PART ADD PRIMARY KEY (P_PARTKEY);
ALTER TABLE SUPPLIER ADD PRIMARY KEY (S_SUPPKEY);
ALTER TABLE CUSTOMER ADD PRIMARY KEY (C_CUSTKEY);
ALTER TABLE [DATE] ADD PRIMARY KEY (D_DATEKEY);
ALTER TABLE LINEORDER ADD PRIMARY KEY (LO_ORDERKEY, LO_LINENUMBER);
```

`./dll/foreignKeys.sql`

```sql
USE PROJEKT_HD_ROWSTORE;
GO

ALTER TABLE LINEORDER ADD CONSTRAINT FK_CustomerLineorder FOREIGN KEY (LO_CUSTKEY) REFERENCES CUSTOMER(C_CUSTKEY);
ALTER TABLE LINEORDER ADD CONSTRAINT FK_PartLineorder FOREIGN KEY (LO_PARTKEY) REFERENCES PART(P_PARTKEY);
ALTER TABLE LINEORDER ADD CONSTRAINT FK_SupplierLineorder FOREIGN KEY (LO_SUPPKEY) REFERENCES SUPPLIER(S_SUPPKEY);
ALTER TABLE LINEORDER ADD CONSTRAINT FK_OrderdateLineorder FOREIGN KEY (LO_ORDERDATE) REFERENCES DATE(D_DATEKEY);
ALTER TABLE LINEORDER ADD CONSTRAINT FK_CommitdateLineorder FOREIGN KEY (LO_COMMITDATE) REFERENCES DATE(D_DATEKEY);

USE PROJEKT_HD_COLUMNSTORE;
GO

ALTER TABLE LINEORDER ADD CONSTRAINT FK_CustomerLineorder FOREIGN KEY (LO_CUSTKEY) REFERENCES CUSTOMER(C_CUSTKEY);
ALTER TABLE LINEORDER ADD CONSTRAINT FK_PartLineorder FOREIGN KEY (LO_PARTKEY) REFERENCES PART(P_PARTKEY);
ALTER TABLE LINEORDER ADD CONSTRAINT FK_SupplierLineorder FOREIGN KEY (LO_SUPPKEY) REFERENCES SUPPLIER(S_SUPPKEY);
ALTER TABLE LINEORDER ADD CONSTRAINT FK_OrderdateLineorder FOREIGN KEY (LO_ORDERDATE) REFERENCES DATE(D_DATEKEY);
ALTER TABLE LINEORDER ADD CONSTRAINT FK_CommitdateLineorder FOREIGN KEY (LO_COMMITDATE) REFERENCES DATE(D_DATEKEY);

USE PROJEKT_HD_COLUMNSTORE_COMPRESSION;
GO

ALTER TABLE LINEORDER ADD CONSTRAINT FK_CustomerLineorder FOREIGN KEY (LO_CUSTKEY) REFERENCES CUSTOMER(C_CUSTKEY);
ALTER TABLE LINEORDER ADD CONSTRAINT FK_PartLineorder FOREIGN KEY (LO_PARTKEY) REFERENCES PART(P_PARTKEY);
ALTER TABLE LINEORDER ADD CONSTRAINT FK_SupplierLineorder FOREIGN KEY (LO_SUPPKEY) REFERENCES SUPPLIER(S_SUPPKEY);
ALTER TABLE LINEORDER ADD CONSTRAINT FK_OrderdateLineorder FOREIGN KEY (LO_ORDERDATE) REFERENCES DATE(D_DATEKEY);
ALTER TABLE LINEORDER ADD CONSTRAINT FK_CommitdateLineorder FOREIGN KEY (LO_COMMITDATE) REFERENCES DATE(D_DATEKEY);
```

`./dll/indexes.sql`

```sql
USE PROJEKT_HD_ROWSTORE;
GO

CREATE NONCLUSTERED INDEX p_category_index
ON PART(p_category);
CREATE NONCLUSTERED INDEX p_brand1_index
ON PART(p_brand1);
CREATE NONCLUSTERED INDEX p_mfgr_index
ON PART(p_mfgr);

CREATE NONCLUSTERED INDEX s_region_index
ON SUPPLIER(s_region);
CREATE NONCLUSTERED INDEX s_nation_index
ON SUPPLIER(s_nation);
CREATE NONCLUSTERED INDEX s_city_index
ON SUPPLIER(s_city);

CREATE NONCLUSTERED INDEX c_region_index
ON CUSTOMER(c_region);
CREATE NONCLUSTERED INDEX c_nation_index
ON CUSTOMER(c_nation);
CREATE NONCLUSTERED INDEX c_city_index
ON CUSTOMER(c_city);

CREATE NONCLUSTERED INDEX d_year_Index
ON DATE(d_year);
CREATE NONCLUSTERED INDEX d_yearmonth_Index
ON DATE(d_yearmonth);
CREATE NONCLUSTERED INDEX d_yearmonthnum_Index
ON DATE(d_yearmonthnum);
CREATE NONCLUSTERED INDEX d_weeknuminyear_Index
ON DATE(d_weeknuminyear);

CREATE NONCLUSTERED INDEX LO_QUANTITY_Index ON LINEORDER(LO_QUANTITY);
CREATE NONCLUSTERED INDEX LO_DISCOUNT_Index ON LINEORDER(LO_DISCOUNT);

USE PROJEKT_HD_COLUMNSTORE;
GO

CREATE NONCLUSTERED INDEX p_category_index
ON PART(p_category);
CREATE NONCLUSTERED INDEX p_brand1_index
ON PART(p_brand1);
CREATE NONCLUSTERED INDEX p_mfgr_index
ON PART(p_mfgr);

CREATE NONCLUSTERED INDEX s_region_index
ON SUPPLIER(s_region);
CREATE NONCLUSTERED INDEX s_nation_index
ON SUPPLIER(s_nation);
CREATE NONCLUSTERED INDEX s_city_index
ON SUPPLIER(s_city);

CREATE NONCLUSTERED INDEX c_region_index
ON CUSTOMER(c_region);
CREATE NONCLUSTERED INDEX c_nation_index
ON CUSTOMER(c_nation);
CREATE NONCLUSTERED INDEX c_city_index
ON CUSTOMER(c_city);

CREATE NONCLUSTERED INDEX d_year_Index
ON DATE(d_year);
CREATE NONCLUSTERED INDEX d_yearmonth_Index
ON DATE(d_yearmonth);
CREATE NONCLUSTERED INDEX d_yearmonthnum_Index
ON DATE(d_yearmonthnum);
CREATE NONCLUSTERED INDEX d_weeknuminyear_Index
ON DATE(d_weeknuminyear);

CREATE NONCLUSTERED INDEX LO_QUANTITY_Index ON LINEORDER(LO_QUANTITY);
CREATE NONCLUSTERED INDEX LO_DISCOUNT_Index ON LINEORDER(LO_DISCOUNT);

USE PROJEKT_HD_COLUMNSTORE_COMPRESSION;
GO

CREATE NONCLUSTERED INDEX p_category_index
ON PART(p_category);
CREATE NONCLUSTERED INDEX p_brand1_index
ON PART(p_brand1);
CREATE NONCLUSTERED INDEX p_mfgr_index
ON PART(p_mfgr);

CREATE NONCLUSTERED INDEX s_region_index
ON SUPPLIER(s_region);
CREATE NONCLUSTERED INDEX s_nation_index
ON SUPPLIER(s_nation);
CREATE NONCLUSTERED INDEX s_city_index
ON SUPPLIER(s_city);

CREATE NONCLUSTERED INDEX c_region_index
ON CUSTOMER(c_region);
CREATE NONCLUSTERED INDEX c_nation_index
ON CUSTOMER(c_nation);
CREATE NONCLUSTERED INDEX c_city_index
ON CUSTOMER(c_city);

CREATE NONCLUSTERED INDEX d_year_Index
ON DATE(d_year);
CREATE NONCLUSTERED INDEX d_yearmonth_Index
ON DATE(d_yearmonth);
CREATE NONCLUSTERED INDEX d_yearmonthnum_Index
ON DATE(d_yearmonthnum);
CREATE NONCLUSTERED INDEX d_weeknuminyear_Index
ON DATE(d_weeknuminyear);

CREATE NONCLUSTERED INDEX LO_QUANTITY_Index ON LINEORDER(LO_QUANTITY);
CREATE NONCLUSTERED INDEX LO_DISCOUNT_Index ON LINEORDER(LO_DISCOUNT);
```

### Benchmark queries

All benchmark queries are stored in `./benchmark/queries`.

Queries 1-3 were created to receive and calculate data based on the specific number of schema dimensions.

`./benchmark/queries/q1.1.sql`

```sql
select sum(lo_extendedprice*lo_discount) as revenue from lineorder, date where lo_orderdate = d_datekey and d_year = 1993 and lo_discount between 1 and 3 and lo_quantity < 25;
```

`./benchmark/queries/q1.2.sql`

```sql
select sum(lo_extendedprice*lo_discount) as revenue from lineorder, date where lo_orderdate = d_datekey and d_yearmonthnum = 199401 and lo_discount between 4 and 6 and lo_quantity between 26 and 35;
```

`./benchmark/queries/q1.3.sql`

```sql
select sum(lo_extendedprice*lo_discount) as revenue from lineorder, date where lo_orderdate = d_datekey and d_weeknuminyear = 6 and d_year = 1994 and lo_discount between 5 and 7 and lo_quantity between 26 and 35;
```

`./benchmark/queries/q2.1.sql`

```sql
select sum(lo_revenue), d_year, p_brand1 from lineorder, date, part, supplier where lo_orderdate = d_datekey and lo_partkey = p_partkey and lo_suppkey = s_suppkey and p_category = 'MFGR#12' and s_region = 'AMERICA' group by d_year, p_brand1 order by d_year, p_brand1;
```

`./benchmark/queries/q2.2.sql`

```sql
select sum(lo_revenue), d_year, p_brand1 from lineorder, date, part, supplier where lo_orderdate = d_datekey and lo_partkey = p_partkey and lo_suppkey = s_suppkey and p_brand1 between 'MFGR#2221' and 'MFGR#2228' and s_region = 'ASIA' group by d_year, p_brand1 order by d_year, p_brand1;
```

`./benchmark/queries/q2.3.sql`

```sql
select sum(lo_revenue), d_year, p_brand1 from lineorder, date, part, supplier where lo_orderdate = d_datekey and lo_partkey = p_partkey and lo_suppkey = s_suppkey and p_brand1 = 'MFGR#2221' and s_region = 'EUROPE' group by d_year, p_brand1 order by d_year, p_brand1;
```

`./benchmark/queries/q3.1.sql`

```sql
select c_nation, s_nation, d_year, sum(lo_revenue) as revenue from customer, lineorder, supplier, date where lo_custkey = c_custkey and lo_suppkey = s_suppkey and lo_orderdate = d_datekey and c_region = 'ASIA' and s_region = 'ASIA' and d_year >= 1992 and d_year <= 1997 group by c_nation, s_nation, d_year order by d_year asc, revenue desc;
```

`./benchmark/queries/q3.2.sql`

```sql
select c_city, s_city, d_year, sum(lo_revenue) as revenue from customer, lineorder, supplier, date where lo_custkey = c_custkey and lo_suppkey = s_suppkey and lo_orderdate = d_datekey and c_nation = 'UNITED STATES' and s_nation = 'UNITED STATES' and d_year >= 1992 and d_year <= 1997 group by c_city, s_city, d_year order by d_year asc, revenue desc;
```

`./benchmark/queries/q3.3.sql`

```sql
select c_city, s_city, d_year, sum(lo_revenue) as revenue from customer, lineorder, supplier, date where lo_custkey = c_custkey and lo_suppkey = s_suppkey and lo_orderdate = d_datekey and (c_city='UNITED KI1' or c_city='UNITED KI5') and (s_city='UNITED KI1' or s_city='UNITED KI5') and d_year >= 1992 and d_year <= 1997 group by c_city, s_city, d_year order by d_year asc, revenue desc;
```

`./benchmark/queries/q3.4.sql`

```sql
select c_city, s_city, d_year, sum(lo_revenue) as revenue from customer, lineorder, supplier, date where lo_custkey = c_custkey and lo_suppkey = s_suppkey and lo_orderdate = d_datekey and (c_city='UNITED KI1' or c_city='UNITED KI5') and (s_city='UNITED KI1' or s_city='UNITED KI5') and d_yearmonth = 'Dec1997' group by c_city, s_city, d_year order by d_year asc, revenue desc;
```

Queries 4 were created to calculate data based on information saved in all database tables.

`./benchmark/queries/q4.1.sql`

```sql
select d_year, c_nation, sum(lo_revenue - lo_supplycost) as profit from date, customer, supplier, part, lineorder where lo_custkey = c_custkey and lo_suppkey = s_suppkey and lo_partkey = p_partkey and lo_orderdate = d_datekey and c_region = 'AMERICA' and s_region = 'AMERICA' and (p_mfgr = 'MFGR#1' or p_mfgr = 'MFGR#2') group by d_year, c_nation order by d_year, c_nation;
```

`./benchmark/queries/q4.2.sql`

```sql
select d_year, s_nation, p_category, sum(lo_revenue - lo_supplycost) as profit from date, customer, supplier, part, lineorder where lo_custkey = c_custkey and lo_suppkey = s_suppkey and lo_partkey = p_partkey and lo_orderdate = d_datekey and c_region = 'AMERICA' and s_region = 'AMERICA' and (d_year = 1997 or d_year = 1998) and (p_mfgr = 'MFGR#1' or p_mfgr = 'MFGR#2') group by d_year, s_nation, p_category order by d_year, s_nation, p_category;
```

`./benchmark/queries/q4.3.sql`

```sql
select d_year, s_city, p_brand1, sum(lo_revenue - lo_supplycost) as profit from date, customer, supplier, part, lineorder where lo_custkey = c_custkey and lo_suppkey = s_suppkey and lo_partkey = p_partkey and lo_orderdate = d_datekey and c_region = 'AMERICA' and s_nation = 'UNITED STATES' and (d_year = 1997 or d_year = 1998) and p_category = 'MFGR#14' group by d_year, s_city, p_brand1 order by d_year, s_city, p_brand1;
```

## Experiment

### Benchmarking

Entire benchmark has been automated using bash scripting. All benchmark queries, that are executed, can be found in folder `./benchmark/queries`. Any other file with extension `.sql` can be put in this directory and added to benchmark workflow.

`./benchmark/benchmark.sh`:

```bash
#!/bin/bash

echo "Executing benchmark queries"

> /output.csv

for f in /queries/*.sql; do

    printf "$f," >> /output.csv
    printf  "\nExecuting $f\n"

    for i in $(seq 1 $ITERATIONS); do

        printf "Iteration $i\n"

        result_time=$(\time -f '%E' sqlcmd -S $SQL_SERVER -d $INITIAL_CATALOG -U sa -P $SA_PASSWORD -i $f 2>&1 > /dev/null)
        echo $result_time

        if [ $i -eq $ITERATIONS ]
        then
            printf "$result_time\n" >> /output.csv
        else
            printf "$result_time," >> /output.csv
        fi

        if [ $? -eq 0 ]
        then
            echo "Query $f executed successfully"
        else
            echo "Unable to execute query $f"
        fi
    done
done

exit 0
```

The default benchmark configuration can be found in `./benchmark/docker-compose.yaml` file.

`./benchmark/docker-compose.yaml`:

```yaml
version: "2.4"

services:
  sqlserver:
    image: mcr.microsoft.com/mssql/server:2019-latest
    environment:
      - 'ACCEPT_EULA=Y'
      - 'SA_PASSWORD=yourStrong(!)Password'
  
  benchmark:
      build:
        context: .
        dockerfile: Dockerfile
      image: projekt_hd-benchmark:latest
      environment:
      - 'SQL_SERVER=sqlserver'
      - 'SA_PASSWORD=yourStrong(!)Password'
      - 'ITERATIONS=2'
```

`./benchmark/Dockerfile`:

```Dockerile
FROM mcr.microsoft.com/mssql-tools
ENV PATH="/opt/mssql-tools/bin:$PATH"
RUN apt-get update && apt-get install -y \
 time \
    && rm -rf /var/lib/apt/lists/*
RUN touch /output.csv
COPY ./queries/ /queries/
COPY benchmark.sh /benchmark.sh
RUN chmod +x /benchmark.sh
CMD /benchmark.sh
```

By default, the following workflow will be executed:

1. Input data will be generated according to the `SCALE` environment variable provided in the file `./data/docker-compose.yaml`.
1. SQL Server will be started in the background.
1. Benchmark will wait for the database to be ready to accept connections.
1. Two databases will be created, one for each storage type.
1. SSIS scripts will be executed for each database to load data.
1. Primary and foreign keys as well as indexes will be created.
1. Database size will be measured for each database.
1. Benchmark queries will be executed against each database, based on `ITERATIONS` environment variable defined in file `./benchmark/docker-compose.yaml`. Output directory for each database type results can be set in files:
   - `./benchmark/docker-compose_columnStore.yaml`
   - `./benchmark/docker-compose_rowStore.yaml`

To execute the benchmark, `./down.ps1 && execute.ps1` command should be used.

```shell
#!/bin/bash

echo "Generating data"

docker-compose -f ./data/docker-compose.yaml up --build
docker-compose -f ./data/docker-compose.yaml down

echo "Setting up database"

docker-compose -f ./database/docker-compose.yaml up -d

echo "Awaiting for database to be ready"

docker-compose -f ./await_for_database/docker-compose.yaml up --build
docker-compose -f ./await_for_database/docker-compose.yaml down

echo "Creating database schema"

docker exec -it sqlserver /opt/mssql-tools/bin/sqlcmd -S sqlserver -U sa -P 'yourStrong(!)Password' -i /dll/setup_rowStore.sql
docker exec -it sqlserver /opt/mssql-tools/bin/sqlcmd -S sqlserver -U sa -P 'yourStrong(!)Password' -i /dll/setup_columnStore.sql

echo "Importing data"

docker-compose -f ./ssis/docker-compose.yaml -f ./ssis/docker-compose_rowStore.yaml up --build
docker-compose -f ./ssis/docker-compose.yaml -f ./ssis/docker-compose_rowStore.yaml down
docker-compose -f ./ssis/docker-compose.yaml -f ./ssis/docker-compose_columnStore.yaml up --build
docker-compose -f ./ssis/docker-compose.yaml -f ./ssis/docker-compose_columnStore.yaml down

echo "Creating primary keys"

docker exec -it sqlserver /opt/mssql-tools/bin/sqlcmd -S sqlserver -U sa -P 'yourStrong(!)Password' -i /dll/primaryKeys.sql

echo "Creating foreign keys"

docker exec -it sqlserver /opt/mssql-tools/bin/sqlcmd -S sqlserver -U sa -P 'yourStrong(!)Password' -i /dll/foreignKeys.sql

echo "Creating indexes"

docker exec -it sqlserver /opt/mssql-tools/bin/sqlcmd -S sqlserver -U sa -P 'yourStrong(!)Password' -i /dll/indexes.sql

echo "Calculating database size"

docker exec -it sqlserver /opt/mssql-tools/bin/sqlcmd -S sqlserver -U sa -P 'yourStrong(!)Password' -Q "SELECT sys.databases.name, CONVERT(VARCHAR,SUM(size)*8/1024)+' MB' AS [Total disk space] FROM sys.databases JOIN sys.master_files ON sys.databases.database_id=sys.master_files.database_id GROUP BY sys.databases.name ORDER BY sys.databases.name" > ./database_size.txt

echo "Executing benchmark"

docker-compose -f ./benchmark/docker-compose.yaml -f ./benchmark/docker-compose_rowStore.yaml up --build
docker-compose -f ./benchmark/docker-compose.yaml -f ./benchmark/docker-compose_rowStore.yaml down
docker-compose -f ./benchmark/docker-compose.yaml -f ./benchmark/docker-compose_columnStore.yaml up --build
docker-compose -f ./benchmark/docker-compose.yaml -f ./benchmark/docker-compose_columnStore.yaml down

docker-compose -f ./database/docker-compose.yaml down

exit 0
```

The output CSV file will contains:

- in the first column: query file name,
- in the following columns: time of each iteration.

```csv
/queries/q1.1.sql,0:05.83,0:00.67
/queries/q1.2.sql,0:00.59,0:00.59
/queries/q1.3.sql,0:00.52,0:00.50
/queries/q2.1.sql,0:01.60,0:00.58
/queries/q2.2.sql,0:00.63,0:00.58
/queries/q2.3.sql,0:00.70,0:00.64
/queries/q3.1.sql,0:01.14,0:00.77
/queries/q3.2.sql,0:00.75,0:00.57
/queries/q3.3.sql,0:00.57,0:00.57
/queries/q3.4.sql,0:00.68,0:00.53
/queries/q4.1.sql,0:00.91,0:00.68
/queries/q4.2.sql,0:00.80,0:00.65
/queries/q4.3.sql,0:00.72,0:00.57
```

### Test environment

A virtual machine for testing was prepared on Google Cloud Services with the following configuration:

- type: ```n1-standard2```

  - 2 virtual processors,
  - 7,5 GB RAM,
  - 200 GB SSD disk,

- Operating system: ```CentOS 8```.

### Database size

All results for database size was measured after index were created but before any benchmark query execution started.

![DatabaseSize.PNG](results/DatabaseSize.PNG)

<center>

| Scale | Row store with indexes | Column store |
| ----- | ---------------------- | ------------ |
| 5     | 8080 MB                | 3728 MB      |
| 10    | 15952 MB               | 6992 MB      |
| 15    | 23824 MB               | 10384 MB     |
| 20    | 31696 MB               | 13712 MB     |
| 30    | 47312 MB               | 20304 MB     |

</center>

### Query execution time

All results was calculated as:

- average of at least 10 different benchmark runs,
- with the removed highest result,
- with the removed lowest result.

For the lowest scale, where almost the entire database can be stored in the RAM memory, it can be seen, that the row store without indexes needs a significant amount of time to execute the simplest queries. For the next queries, where more dimensions are used, the row store executes faster or nearly the same as the column store. For queries, where an entire database is used to calculate the result, the row store can be much faster then the column store.The row store with indexes is multiple times faster then other considered storage solutions.

![scale5.PNG](results/scale5.PNG)

| Query     | Row store - average | Row store - standard deviation | Row store with indexes - average | Row store with indexes - standard deviation | Column store - average | Column store - standard deviation |
| --------- | ------------------- | ------------------------------ | -------------------------------- | ------------------------------------------- | ---------------------- | --------------------------------- |
| Query 1.1 | 02:12.32 min        | 00:44.72 min                   | 00:08.12 min                     | 00:10.42 min                                | 00:02.72 min           | 00:00.32 min                      |
| Query 1.2 | 04:00.42 min        | 00:00.62 min                   | 00:02.82 min                     | 00:00.12 min                                | 00:02.32 min           | 00:00.52 min                      |
| Query 1.3 | 00:59.22 min        | 01:17.52 min                   | 00:02.92 min                     | 00:00.12 min                                | 00:01.42 min           | 00:00.72 min                      |
| Query 2.1 | 00:40.92 min        | 00:14.52 min                   | 00:03.62 min                     | 00:00.52 min                                | 00:08.62 min           | 00:04.72 min                      |
| Query 2.2 | 01:14.72 min        | 00:21.12 min                   | 00:03.22 min                     | 00:00.12 min                                | 00:12.52 min           | 00:01.22 min                      |
| Query 2.3 | 00:09.82 min        | 00:18.72 min                   | 00:03.52 min                     | 00:00.22 min                                | 00:12.72 min           | 00:01.62 min                      |
| Query 3.1 | 00:03.12 min        | 00:00.32 min                   | 00:03.12 min                     | 00:00.12 min                                | 00:13.82 min           | 00:00.42 min                      |
| Query 3.2 | 00:02.82 min        | 00:00.92 min                   | 00:02.72 min                     | 00:00.12 min                                | 00:13.22 min           | 00:01.12 min                      |
| Query 3.3 | 00:40.82 min        | 00:29.72 min                   | 00:02.72 min                     | 00:00.12 min                                | 00:13.32 min           | 00:00.42 min                      |
| Query 3.4 | 00:07.52 min        | 00:11.42 min                   | 00:02.72 min                     | 00:00.72 min                                | 00:10.42 min           | 00:03.42 min                      |
| Query 4.1 | 00:03.22 min        | 00:00.42 min                   | 00:03.12 min                     | 00:00.12 min                                | 00:19.92 min           | 00:01.92 min                      |
| Query 4.2 | 00:07.92 min        | 00:04.62 min                   | 00:03.22 min                     | 00:00.22 min                                | 00:18.72 min           | 00:02.32 min                      |
| Query 4.3 | 00:34.42 min        | 00:24.82 min                   | 00:02.72 min                     | 00:00.12 min                                | 00:19.72 min           | 00:00.82 min                      |

The pattern can be seen with an increase of the SSB scale. When the database size is greater then the available RAM memory, execution time is almost the same for all queries, no matter how complicated it is. Indexes can increate row store performance by 30-40%. The column store needs only 5-10% of the row store execution time.

![scale10.PNG](results/scale10.PNG)

| Query     | Row store - average | Row store - standard deviation | Row store with indexes - average | Row store with indexes - standard deviation | Column store - average | Column store - standard deviation |
| --------- | ------------------- | ------------------------------ | -------------------------------- | ------------------------------------------- | ---------------------- | --------------------------------- |
| Query 1.1 | 06:51.12 min        | 01:59.52 min                   | 05:14.82 min                     | 00:00.12 min                                | 00:20.32 min           | 00:00.62 min                      |
| Query 1.2 | 08:01.12 min        | 00:00.82 min                   | 05:14.72 min                     | 00:00.12 min                                | 00:20.22 min           | 00:00.32 min                      |
| Query 1.3 | 08:01.62 min        | 00:01.02 min                   | 05:14.82 min                     | 00:00.22 min                                | 00:20.32 min           | 00:00.12 min                      |
| Query 2.1 | 08:00.82 min        | 00:00.62 min                   | 05:14.72 min                     | 00:00.22 min                                | 00:28.82 min           | 00:00.42 min                      |
| Query 2.2 | 08:01.62 min        | 00:00.92 min                   | 05:14.82 min                     | 00:00.22 min                                | 00:28.72 min           | 00:00.42 min                      |
| Query 2.3 | 08:02.52 min        | 00:01.72 min                   | 05:14.62 min                     | 00:00.12 min                                | 00:28.72 min           | 00:00.32 min                      |
| Query 3.1 | 08:03.82 min        | 00:04.42 min                   | 05:14.82 min                     | 00:00.22 min                                | 00:28.92 min           | 00:00.52 min                      |
| Query 3.2 | 08:04.22 min        | 00:02.92 min                   | 05:14.82 min                     | 00:00.32 min                                | 00:28.62 min           | 00:00.92 min                      |
| Query 3.3 | 08:02.72 min        | 00:01.82 min                   | 05:14.72 min                     | 00:00.12 min                                | 00:28.52 min           | 00:00.82 min                      |
| Query 3.4 | 08:01.22 min        | 00:00.52 min                   | 05:14.72 min                     | 00:00.12 min                                | 00:29.62 min           | 00:00.22 min                      |
| Query 4.1 | 08:01.22 min        | 00:00.52 min                   | 05:14.72 min                     | 00:00.22 min                                | 00:42.42 min           | 00:00.12 min                      |
| Query 4.2 | 08:02.52 min        | 00:01.82 min                   | 05:14.72 min                     | 00:00.12 min                                | 00:42.42 min           | 00:00.92 min                      |
| Query 4.3 | 08:01.62 min        | 00:00.82 min                   | 05:14.82 min                     | 00:00.12 min                                | 00:41.72 min           | 00:00.82 min                      |

![scale15.PNG](results/scale15.PNG)

| Query     | Row store - average | Row store - standard deviation | Row store with indexes - average | Row store with indexes - standard deviation | Column store - average | Column store - standard deviation |
| --------- | ------------------- | ------------------------------ | -------------------------------- | ------------------------------------------- | ---------------------- | --------------------------------- |
| Query 1.1 | 11:31.52 min        | 02:13.42 min                   | 08:10.62 min                     | 00:00.42 min                                | 00:30.42 min           | 00:00.32 min                      |
| Query 1.2 | 12:10.22 min        | 00:03.42 min                   | 08:09.82 min                     | 00:00.12 min                                | 00:29.52 min           | 00:01.12 min                      |
| Query 1.3 | 12:10.72 min        | 00:03.82 min                   | 08:09.92 min                     | 00:00.12 min                                | 00:30.32 min           | 00:00.22 min                      |
| Query 2.1 | 12:09.52 min        | 00:05.42 min                   | 08:10.22 min                     | 00:00.22 min                                | 00:43.12 min           | 00:00.12 min                      |
| Query 2.2 | 12:07.62 min        | 00:01.72 min                   | 08:09.82 min                     | 00:00.12 min                                | 00:42.82 min           | 00:00.42 min                      |
| Query 2.3 | 12:18.72 min        | 00:15.22 min                   | 08:09.82 min                     | 00:00.12 min                                | 00:43.12 min           | 00:00.12 min                      |
| Query 3.1 | 12:11.82 min        | 00:06.62 min                   | 08:09.82 min                     | 00:00.22 min                                | 00:43.12 min           | 00:00.12 min                      |
| Query 3.2 | 12:11.12 min        | 00:06.12 min                   | 08:09.72 min                     | 00:00.12 min                                | 00:43.12 min           | 00:00.92 min                      |
| Query 3.3 | 12:09.92 min        | 00:05.42 min                   | 08:09.82 min                     | 00:00.12 min                                | 00:42.82 min           | 00:00.52 min                      |
| Query 3.4 | 12:09.92 min        | 00:05.22 min                   | 08:09.82 min                     | 00:00.12 min                                | 00:43.12 min           | 00:00.62 min                      |
| Query 4.1 | 12:11.82 min        | 00:05.62 min                   | 08:09.92 min                     | 00:00.32 min                                | 01:02.72 min           | 00:00.12 min                      |
| Query 4.2 | 12:10.22 min        | 00:04.62 min                   | 08:09.82 min                     | 00:00.22 min                                | 01:02.72 min           | 00:00.92 min                      |
| Query 4.3 | 12:09.62 min        | 00:01.42 min                   | 08:11.32 min                     | 00:02.62 min                                | 01:02.72 min           | 00:00.52 min                      |

![scale20.PNG](results/scale20.PNG)

| Query     | Row store - average | Row store - standard deviation | Row store with indexes - average | Row store with indexes - standard deviation | Column store - average | Column store - standard deviation |
| --------- | ------------------- | ------------------------------ | -------------------------------- | ------------------------------------------- | ---------------------- | --------------------------------- |
| Query 1.1 | 16:19.82 min        | 02:28.42 min                   | 10:38.52 min                     | 00:00.32 min                                | 00:41.12 min           | 00:00.32 min                      |
| Query 1.2 | 16:06.42 min        | 00:01.42 min                   | 10:38.22 min                     | 00:00.22 min                                | 00:40.92 min           | 00:00.42 min                      |
| Query 1.3 | 16:05.42 min        | 00:03.22 min                   | 10:38.12 min                     | 00:00.12 min                                | 00:40.92 min           | 00:00.22 min                      |
| Query 2.1 | 16:09.22 min        | 00:02.72 min                   | 08:58.62 min                     | 02:54.92 min                                | 00:57.82 min           | 00:00.92 min                      |
| Query 2.2 | 16:14.12 min        | 00:17.42 min                   | 10:38.22 min                     | 00:00.22 min                                | 00:57.82 min           | 00:00.72 min                      |
| Query 2.3 | 16:04.82 min        | 00:01.52 min                   | 10:38.22 min                     | 00:00.12 min                                | 00:57.82 min           | 00:00.42 min                      |
| Query 3.1 | 16:05.92 min        | 00:03.72 min                   | 10:41.12 min                     | 00:04.42 min                                | 00:58.12 min           | 00:00.52 min                      |
| Query 3.2 | 16:05.82 min        | 00:02.72 min                   | 10:38.22 min                     | 00:00.22 min                                | 00:58.12 min           | 00:00.72 min                      |
| Query 3.3 | 16:06.82 min        | 00:01.82 min                   | 10:38.22 min                     | 00:00.12 min                                | 00:58.12 min           | 00:00.42 min                      |
| Query 3.4 | 16:05.72 min        | 00:02.22 min                   | 10:38.22 min                     | 00:00.22 min                                | 00:58.12 min           | 00:00.62 min                      |
| Query 4.1 | 16:13.62 min        | 00:12.82 min                   | 10:38.32 min                     | 00:00.32 min                                | 01:24.12 min           | 00:00.12 min                      |
| Query 4.2 | 16:05.92 min        | 00:01.52 min                   | 10:38.62 min                     | 00:00.52 min                                | 01:24.12 min           | 00:00.92 min                      |
| Query 4.3 | 16:06.42 min        | 00:01.62 min                   | 10:38.42 min                     | 00:00.32 min                                | 01:24.22 min           | 00:00.82 min                      |

![scale30.PNG](results/scale30.PNG)

| Query     | Row store - average | Row store - standard deviation | Row store with indexes - average | Row store with indexes - standard deviation | Column store - average | Column store - standard deviation |
| --------- | ------------------- | ------------------------------ | -------------------------------- | ------------------------------------------- | ---------------------- | --------------------------------- |
| Query 1.1 | 25:44.22 min        | 02:25.62 min                   | 16:10.72 min                     | 00:00.32 min                                | 01:00.42 min           | 00:00.12 min                      |
| Query 1.2 | 24:24.72 min        | 00:08.92 min                   | 16:10.62 min                     | 00:00.22 min                                | 01:00.32 min           | 00:00.52 min                      |
| Query 1.3 | 24:27.62 min        | 00:06.52 min                   | 16:10.52 min                     | 00:00.12 min                                | 00:51.82 min           | 00:08.92 min                      |
| Query 2.1 | 24:30.82 min        | 00:06.72 min                   | 16:10.62 min                     | 00:00.12 min                                | 01:00.92 min           | 00:11.62 min                      |
| Query 2.2 | 24:27.42 min        | 00:07.62 min                   | 16:10.62 min                     | 00:00.22 min                                | 00:26.32 min           | 00:09.82 min                      |
| Query 2.3 | 24:41.82 min        | 00:26.82 min                   | 16:13.52 min                     | 00:05.12 min                                | 00:05.72 min           | 00:03.62 min                      |
| Query 3.1 | 24:32.42 min        | 00:06.92 min                   | 16:11.22 min                     | 00:00.72 min                                | 00:08.42 min           | 00:05.52 min                      |
| Query 3.2 | 24:29.42 min        | 00:03.52 min                   | 16:10.72 min                     | 00:00.22 min                                | 00:01.92 min           | 00:00.92 min                      |
| Query 3.3 | 24:28.62 min        | 00:03.92 min                   | 16:10.62 min                     | 00:00.32 min                                | 00:01.52 min           | 00:00.72 min                      |
| Query 3.4 | 24:35.22 min        | 00:13.52 min                   | 16:10.62 min                     | 00:00.22 min                                | 00:00.72 min           | 00:00.62 min                      |
| Query 4.1 | 24:24.82 min        | 00:07.12 min                   | 16:10.92 min                     | 00:00.62 min                                | 00:18.52 min           | 00:04.22 min                      |
| Query 4.2 | 24:30.42 min        | 00:02.22 min                   | 16:10.72 min                     | 00:00.22 min                                | 00:08.52 min           | 00:02.32 min                      |
| Query 4.3 | 24:27.52 min        | 00:08.82 min                   | 16:13.12 min                     | 00:04.62 min                                | 00:02.82 min           | 00:00.32 min                      |

## Summary

The assumed project goal in the form of comparison of different data storage configuration have been achieved. Created scripts and Dockerfiles allow to execute a fully automated experiment on any configuration. The parameterized setup for creating data sets and iterations allows to perform the experiment multiple times on a different scale.
Obtained results prove that the column store index on Microsoft SQL Server is definitely a better solution to store data in the snowflake data warehouse model, rather than row storage. The column store database size is significantly smaller compared to the row store with indexes solution. Query execution time is also in favour to the column data storage.

## Bibliography

- Star Schema Benchmark, Revision 3, June 5, 2009, Pat O'Neil, Betty O'Neil, Xuedong Chen: <https://www.cs.umb.edu/~poneil/StarSchemaB.PDF>
- Column store indexes: Overview <https://docs.microsoft.com/en-us/sql/relational-databases/indexes/columnstore-indexes-overview?view=sql-server-ver15>
- SSB source code generator: <https://github.com/eyalroz/ssb-dbgen/>

#!/bin/bash

sleep_time="1s"
retries=0
while sqlcmd -S $SQL_SERVER -U sa -P $SA_PASSWORD -Q "SELECT 1" >> /dev/null; ret=$?; retries=$((retries+1)); [[ ret -ne 0 ]]; do
    echo "Try $retries: database not ready to accept connections, sleeping for $sleep_time"
    sleep $sleep_time
done

echo "Try $retries: database is ready to accept connections"

sleep 30s

exit 0
#!/bin/bash

docker-compose -f ./data/docker-compose.yaml build --no-cache
docker-compose -f ./await_for_database/docker-compose.yaml build --no-cache
docker-compose -f ./ssis/docker-compose.yaml build --no-cache
docker-compose -f ./benchmark/docker-compose.yaml build --no-cache

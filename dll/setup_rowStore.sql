USE [master];
GO

DROP DATABASE IF EXISTS PROJEKT_HD_ROWSTORE;
GO

CREATE DATABASE PROJEKT_HD_ROWSTORE;
GO

USE PROJEKT_HD_ROWSTORE;
GO
ALTER DATABASE PROJEKT_HD_ROWSTORE SET RECOVERY SIMPLE;
GO
CREATE TABLE PART
(
    P_PARTKEY int NOT NULL,
    P_NAME varchar(50),
    P_MFGR varchar(50),
    P_CATEGORY varchar(50),
    P_BRAND1 varchar(50),
    P_COLOR varchar(50),
    P_TYPE varchar(50),
    P_SIZE numeric,
    P_CONTAINER varchar(50)
);

CREATE TABLE SUPPLIER
(
    S_SUPPKEY int NOT NULL,
    S_NAME varchar(50),
    S_ADDRESS varchar(50),
    S_CITY varchar(50),
    S_NATION varchar(50),
    S_REGION varchar(50),
    S_PHONE varchar(50)
);

CREATE TABLE CUSTOMER
(
    C_CUSTKEY int NOT NULL,
    C_NAME varchar(50),
    C_ADDRESS varchar(50),
    C_CITY varchar(50),
    C_NATION varchar(50),
    C_REGION varchar(50),
    C_PHONE varchar(50),
    C_MKTSEGMENT varchar(50),
);

CREATE TABLE DATE
(
    D_DATEKEY int NOT NULL,
    D_DATE varchar(50),
    D_DAYOFWEEK varchar(50),
    D_MONTH varchar(50),
    D_YEAR int,
    D_YEARMONTHNUM int,
    D_YEARMONTH varchar(50),
    D_DAYNUMINWEEK int,
    D_DAYNUMINMONTH int,
    D_DAYNUMINYEAR int,
    D_MONTHNUMINYEAR int,
    D_WEEKNUMINYEAR int,
    D_SELLINGSEASON varchar(50),
    D_LASTDAYINWEEKFL bit,
    D_LASTDAYINMONTHFL bit,
    D_HOLIDAYFL bit,
    D_WEEKDAYFL bit
);

CREATE TABLE LINEORDER
(
    LO_ORDERKEY int NOT NULL,
    LO_LINENUMBER int NOT NULL,
    LO_CUSTKEY int,
    LO_PARTKEY int,
    LO_SUPPKEY int,
    LO_ORDERDATE int,
    LO_ORDERPRIORITY varchar(50),
    LO_SHIPPRIORITY varchar(50),
    LO_QUANTITY numeric,
    LO_EXTENDEDPRICE numeric,
    LO_ORDTOTALPRICE numeric,
    LO_DISCOUNT numeric,
    LO_REVENUE numeric,
    LO_SUPPLYCOST numeric,
    LO_TAX numeric,
    LO_COMMITDATE int,
    LO_SHIPMODE varchar(50)
);

GO
USE PROJEKT_HD_ROWSTORE;
GO

CREATE NONCLUSTERED INDEX p_category_index
ON PART(p_category);
CREATE NONCLUSTERED INDEX p_brand1_index
ON PART(p_brand1);
CREATE NONCLUSTERED INDEX p_mfgr_index
ON PART(p_mfgr);

CREATE NONCLUSTERED INDEX s_region_index
ON SUPPLIER(s_region);
CREATE NONCLUSTERED INDEX s_nation_index
ON SUPPLIER(s_nation);
CREATE NONCLUSTERED INDEX s_city_index
ON SUPPLIER(s_city);

CREATE NONCLUSTERED INDEX c_region_index
ON CUSTOMER(c_region);
CREATE NONCLUSTERED INDEX c_nation_index
ON CUSTOMER(c_nation);
CREATE NONCLUSTERED INDEX c_city_index
ON CUSTOMER(c_city);

CREATE NONCLUSTERED INDEX d_year_Index
ON DATE(d_year);
CREATE NONCLUSTERED INDEX d_yearmonth_Index
ON DATE(d_yearmonth);
CREATE NONCLUSTERED INDEX d_yearmonthnum_Index
ON DATE(d_yearmonthnum);
CREATE NONCLUSTERED INDEX d_weeknuminyear_Index
ON DATE(d_weeknuminyear);

CREATE NONCLUSTERED INDEX LO_QUANTITY_Index ON LINEORDER(LO_QUANTITY);
CREATE NONCLUSTERED INDEX LO_DISCOUNT_Index ON LINEORDER(LO_DISCOUNT);

USE PROJEKT_HD_COLUMNSTORE;
GO

CREATE NONCLUSTERED INDEX p_category_index
ON PART(p_category);
CREATE NONCLUSTERED INDEX p_brand1_index
ON PART(p_brand1);
CREATE NONCLUSTERED INDEX p_mfgr_index
ON PART(p_mfgr);

CREATE NONCLUSTERED INDEX s_region_index
ON SUPPLIER(s_region);
CREATE NONCLUSTERED INDEX s_nation_index
ON SUPPLIER(s_nation);
CREATE NONCLUSTERED INDEX s_city_index
ON SUPPLIER(s_city);

CREATE NONCLUSTERED INDEX c_region_index
ON CUSTOMER(c_region);
CREATE NONCLUSTERED INDEX c_nation_index
ON CUSTOMER(c_nation);
CREATE NONCLUSTERED INDEX c_city_index
ON CUSTOMER(c_city);

CREATE NONCLUSTERED INDEX d_year_Index
ON DATE(d_year);
CREATE NONCLUSTERED INDEX d_yearmonth_Index
ON DATE(d_yearmonth);
CREATE NONCLUSTERED INDEX d_yearmonthnum_Index
ON DATE(d_yearmonthnum);
CREATE NONCLUSTERED INDEX d_weeknuminyear_Index
ON DATE(d_weeknuminyear);

CREATE NONCLUSTERED INDEX LO_QUANTITY_Index ON LINEORDER(LO_QUANTITY);
CREATE NONCLUSTERED INDEX LO_DISCOUNT_Index ON LINEORDER(LO_DISCOUNT);

USE PROJEKT_HD_COLUMNSTORE_COMPRESSION;
GO

CREATE NONCLUSTERED INDEX p_category_index
ON PART(p_category);
CREATE NONCLUSTERED INDEX p_brand1_index
ON PART(p_brand1);
CREATE NONCLUSTERED INDEX p_mfgr_index
ON PART(p_mfgr);

CREATE NONCLUSTERED INDEX s_region_index
ON SUPPLIER(s_region);
CREATE NONCLUSTERED INDEX s_nation_index
ON SUPPLIER(s_nation);
CREATE NONCLUSTERED INDEX s_city_index
ON SUPPLIER(s_city);

CREATE NONCLUSTERED INDEX c_region_index
ON CUSTOMER(c_region);
CREATE NONCLUSTERED INDEX c_nation_index
ON CUSTOMER(c_nation);
CREATE NONCLUSTERED INDEX c_city_index
ON CUSTOMER(c_city);

CREATE NONCLUSTERED INDEX d_year_Index
ON DATE(d_year);
CREATE NONCLUSTERED INDEX d_yearmonth_Index
ON DATE(d_yearmonth);
CREATE NONCLUSTERED INDEX d_yearmonthnum_Index
ON DATE(d_yearmonthnum);
CREATE NONCLUSTERED INDEX d_weeknuminyear_Index
ON DATE(d_weeknuminyear);

CREATE NONCLUSTERED INDEX LO_QUANTITY_Index ON LINEORDER(LO_QUANTITY);
CREATE NONCLUSTERED INDEX LO_DISCOUNT_Index ON LINEORDER(LO_DISCOUNT);
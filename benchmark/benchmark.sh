#!/bin/bash

echo "Executing benchmark queries"

> /output.csv

for f in /queries/*.sql; do
    
    printf "$f," >> /output.csv
    printf  "\nExecuting $f\n"
    
    for i in $(seq 1 $ITERATIONS); do 
        
        printf "Iteration $i\n"
        
        result_time=$(\time -f '%E' sqlcmd -S $SQL_SERVER -d $INITIAL_CATALOG -U sa -P $SA_PASSWORD -i $f 2>&1 > /dev/null)
        echo $result_time
        
        if [ $i -eq $ITERATIONS ]
        then
            printf "$result_time\n" >> /output.csv
        else
            printf "$result_time," >> /output.csv
        fi

        if [ $? -eq 0 ] 
        then
            echo "Query $f executed successfully"
        else
            echo "Unable to execute query $f"
        fi
    done
done

exit 0
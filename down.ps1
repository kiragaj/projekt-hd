#!/bin/bash

docker-compose -f ./await_for_database/docker-compose.yaml down
docker-compose -f ./benchmark/docker-compose.yaml down
docker-compose -f ./data/docker-compose.yaml down
docker-compose -f ./database/docker-compose.yaml down
docker-compose -f ./ssis/docker-compose.yaml down
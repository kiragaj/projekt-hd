#!/bin/bash

echo "Generating data"

docker-compose -f ./data/docker-compose.yaml up --build
docker-compose -f ./data/docker-compose.yaml down

echo "Setting up database"

docker-compose -f ./database/docker-compose.yaml up -d

echo "Awaiting for database to be ready"

docker-compose -f ./await_for_database/docker-compose.yaml up --build
docker-compose -f ./await_for_database/docker-compose.yaml down

echo "Creating database schema"

docker exec -it sqlserver /opt/mssql-tools/bin/sqlcmd -S sqlserver -U sa -P 'yourStrong(!)Password' -i /dll/setup_rowStore.sql
docker exec -it sqlserver /opt/mssql-tools/bin/sqlcmd -S sqlserver -U sa -P 'yourStrong(!)Password' -i /dll/setup_columnStore.sql

echo "Importing data"

docker-compose -f ./ssis/docker-compose.yaml -f ./ssis/docker-compose_rowStore.yaml up --build
docker-compose -f ./ssis/docker-compose.yaml -f ./ssis/docker-compose_rowStore.yaml down
docker-compose -f ./ssis/docker-compose.yaml -f ./ssis/docker-compose_columnStore.yaml up --build
docker-compose -f ./ssis/docker-compose.yaml -f ./ssis/docker-compose_columnStore.yaml down

echo "Creating primary keys"

docker exec -it sqlserver /opt/mssql-tools/bin/sqlcmd -S sqlserver -U sa -P 'yourStrong(!)Password' -i /dll/primaryKeys.sql

echo "Creating foreign keys"

docker exec -it sqlserver /opt/mssql-tools/bin/sqlcmd -S sqlserver -U sa -P 'yourStrong(!)Password' -i /dll/foreignKeys.sql

echo "Creating indexes"

docker exec -it sqlserver /opt/mssql-tools/bin/sqlcmd -S sqlserver -U sa -P 'yourStrong(!)Password' -i /dll/indexes.sql

echo "Calculating database size"

docker exec -it sqlserver /opt/mssql-tools/bin/sqlcmd -S sqlserver -U sa -P 'yourStrong(!)Password' -Q "SELECT sys.databases.name, CONVERT(VARCHAR,SUM(size)*8/1024)+' MB' AS [Total disk space] FROM sys.databases JOIN sys.master_files ON sys.databases.database_id=sys.master_files.database_id GROUP BY sys.databases.name ORDER BY sys.databases.name" > ./database_size.txt

echo "Executing benchmark"

docker-compose -f ./benchmark/docker-compose.yaml -f ./benchmark/docker-compose_rowStore.yaml up --build
docker-compose -f ./benchmark/docker-compose.yaml -f ./benchmark/docker-compose_rowStore.yaml down
docker-compose -f ./benchmark/docker-compose.yaml -f ./benchmark/docker-compose_columnStore.yaml up --build
docker-compose -f ./benchmark/docker-compose.yaml -f ./benchmark/docker-compose_columnStore.yaml down

docker-compose -f ./database/docker-compose.yaml down

exit 0
